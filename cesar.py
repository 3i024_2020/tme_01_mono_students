#!/usr/bin/env python3
import sys

#######################################
# CONSIGNE
# Modifier uniquement les fonctions suivantes :
# - chiffrer_cesar
# - dechiffrer_cesar
#######################################

def chiffrer_cesar(message_clair, clef):
    # MODIFIER LE CODE ICI
    message_chiffre = message_clair
    return message_chiffre

def dechiffrer_cesar(message_clair, clef):
    # MODIFIER LE CODE ICI
    message_chiffre = message_clair
    return message_chiffre

# NE PAS MODIFIER APRES CETTE LIGNE

def usage():
    print ("Usage : python cesar.py clef c/d phrase",file=sys.stderr)
    print ("Exemple 1 : python cesar.py E c ALICE",file=sys.stderr)
    print ("\t > EPMGI",file=sys.stderr)
    print ("Exemple 2 : python cesar.py E d EPMGI",file=sys.stderr)
    print ("\t > ALICE",file=sys.stderr)
    sys.exit(1)

if len(sys.argv) != 4:
    usage()
    
clef = sys.argv[1]
operation = sys.argv[2]
phrase = sys.argv[3]

if operation == 'c' : 
    phrase2 = chiffrer_cesar(phrase, clef)
elif operation == 'd' :
    phrase2 = dechiffrer_cesar(phrase, clef)
else:
    usage()
print(phrase2)
